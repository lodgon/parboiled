package com.lodgon.parboiled.examples.indenting;

import static com.lodgon.parboiled.support.ParseTreeUtils.printNodeTree;

import com.lodgon.parboiled.Parboiled;
import com.lodgon.parboiled.buffers.IndentDedentInputBuffer;
import com.lodgon.parboiled.errors.ErrorUtils;
import com.lodgon.parboiled.parserunners.ReportingParseRunner;
import com.lodgon.parboiled.support.ParsingResult;

public class Main {

    public static void main(String[] args) {
        SimpleIndent parser = Parboiled.createParser(SimpleIndent.class);
        String input = "NodeA \n\tNodeB\n\tNodeC \n\t\tNodeD \nNodeE";

        ParsingResult<?> result = new ReportingParseRunner(parser.Parent())
                .run(new IndentDedentInputBuffer(input.toCharArray(), 2, ";", true, true));

        if (!result.parseErrors.isEmpty()) {
            System.out.println(ErrorUtils.printParseError(result.parseErrors
                    .get(0)));
        } else {
            System.out.println("NodeTree: " + printNodeTree(result) + '\n');
            Object value = result.parseTreeRoot.getValue();
            System.out.println(value.toString());
        }

    }
}